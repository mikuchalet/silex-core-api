<?php

namespace Mkch\CoreApi\Model;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class GenericModel
{
    private $modelName;
    private $primaryKey;
    private $properties = array();

    public function __construct($modelName, $arrayModel)
    {
        if(!empty($arrayModel)) {
            $this->modelName = $modelName;
            $this->load($arrayModel);
        }
    }

    public function load($arrayFields)
    {
        try {

            //Init primaryKey
            if(isset($arrayFields['primaryKey']) && !empty($arrayFields['primaryKey'])) {
                $this->setPrimaryKey($arrayFields['primaryKey']);
            }
            else {
                throw new \Exception('You need to declare a primary Key');
            }

            //Init fields
            foreach($arrayFields['fields'] as $name => $type) {
                $this->addProperty($name, $type);
            }

        }
        catch(\Exception $e) {
            throw $e;
        }
    }

    public function verifyModel()
    {
        try {

            foreach($this->properties as $name => $params) {
                // dump($name);
                if($this->isProperty($name)) {
                    //IF not nullable
                    if(isset($params['nullable']) && $params['nullable'] === false && $this->{$name} !== NULL) {

                        //Check format
                        if($params['format']) {

                            //Check formats
                            if(($params['format'] === 'integer' && is_numeric($this->{$name}))
                                || ($params['format'] === 'boolean' && is_bool($this->{$name}))
                                || ($params['format'] === 'string' && is_string($this->{$name}))
                                || ($params['format'] === 'Model' && !$this->isRelatedModel($name))
                            ) {

                                //Cast property as string for getting length and compare with length parameter
                                if(!empty($params['length']) && (strlen((string)$this->{$name}) > $params['length'])) {
                                    throw new \Exception("Value of property $name is out of length", 504);
                                }
                            } else {
                                throw new \Exception("Wrong type for property $name", 504);
                            }
                        }

                    } else {
                        if(isset($params['default']) && !empty($params['default'])) {
                            //IF property null and default value declared, set it
                            $this->{$name} = $params['default'];
                        }
                    }
                } else {
                    throw new \Exception("Property $name does not exist", 504);
                }

            }

            return true;

        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function handleRelations()
    {
        //Check if relations
        //Register GenericModel for relational model
    }


    //Used for setting new property
    public function __set($property, $value)
    {
        // Vérification d'existence de la propriété
        if($this->isProperty($property)) {
            $this->{$property} = $value;
        } elseif ($model = $this->isRelatedModel($property)) {
            $this->{$model} = $value;
        } else {
            throw new \Exception("Trying to set unexistent property $property");
        }

        return $this;
    }

    //Used for getting property
    public function __get($name)
    {
        // Vérification d'existence de la propriété
        if($this->isProperty($name)) {
            return $this->{$name};
        } else {
            throw new \Exception("Trying to get unexistent property $name");
        }

        // TODO : Model relationnel


    }

    public function getModelName()
    {
        return $this->modelName;
    }

    public function setModelName($modelName)
    {
        $this->modelName = $modelName;
        return $this;
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    public function addProperty($name, $params)
    {
        //Add property
        $this->properties[$name] = $params;

        //Init
        $this->{$name} = null;
    }

    public function isProperty($property)
    {
        //Check if is primaryKey
        if($property !== key($this->primaryKey)) {
            //If not, check if is property
            if(!array_key_exists($property, $this->properties)) {
                return false;
            }
        }
        return true;
    }


    public function isRelatedModel($property)
    {
        //Check si ça finit par _id
        if(substr($property, -3) === '_id') {
            $model = substr($property, 0, strlen($property)-3);

            //Check si ça correspond à une propriété de type Model
            if(array_key_exists($model, $this->properties) && $this->properties[$model]['format'] === 'Model') {
                return $model;
            }
        }

        return false;
    }


    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;

        //Init PK property
        $this->{key($primaryKey)} = null;
    }

}
