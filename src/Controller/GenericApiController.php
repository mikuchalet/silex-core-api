<?php

namespace Mkch\CoreApi\Controller;

use Mkch\CoreApi\Application;

use Mkch\CoreApi\Model\GenericModel;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Mkch\CoreApi\Transformer;

use Mkch\CoreApi\Transformer\GenericTransformer;

use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Serializer\ArraySerializer;


/**
*   Generic API Controller
*   Includes methods for an API (get/getAll/update/delete/create)
*   Works with GenericModel
*/
class GenericApiController implements InterfaceApiController
{

    private $apiName;
    private $transformer;
    private $includes;

    /***** HTTP API Methods ******/
    public function getAction(Application $app, Request $request, $id)
    {
        try
        {
            $this->setApiName($request);
            $this->setIncludes($request);

            // GenericModel
            if($data = $app['repo.'.$this->apiName]->findById($id))
            {
                $includes = $request->get('include');

                // GenericModel
                $formated = $this->setItem($app, $data, 'Array', $includes);
                // Json

                return $this->extendedResponse(200,  $this->apiName." found this id", $formated);
            } else {
                throw new \Exception($this->apiName." not found with this id", 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }
    }

    public function getAllAction(Application $app, Request $request)
    {
        try {
            $this->setApiName($request);
            $this->setIncludes($request);

            if ($data = $app['repo.'.$this->apiName]->findAll())
            {
                //dump($data);
                $includes = $request->get('include');
                $formated = $this->setCollection($app, $data, 'Array', $includes);
                //dump($formated);die;
                return $this->extendedResponse(200, $this->apiName."s found", $formated);
            } else {
                throw new \Exception($this->apiName."s not found", 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }
    }


    public function createAction(Application $app, Request $request)
    {
        try {
            $this->setApiName($request);

            // GenericModel
            $object = new GenericModel($this->apiName, $app['model.' . $this->apiName]);

            // Parsing de la request
            foreach($request->query->all() as $param => $value) {
                $object->__set($param, $value);
            }

            if($object->verifyModel() === true) {
                $app['repo.'.$this->apiName]->save($object);

                return $this->extendedResponse(200, $this->apiName." created");
            } else {
                throw new \Exception("Wrong parameters", 500);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }
    }


    public function updateAction(Application $app, Request $request, $id)
    {
        try
        {
            $this->setApiName($request);

            if($object = $app['repo.'.$this->apiName]->findById($id))
            {

                // Parsing de la request
                foreach($request->query->all() as $param => $value) {
                    $object->__set($param, $value);
                }

                if($object->verifyModel() === true) {

                    $app['repo.'.$this->apiName]->save($object);

                    return $this->extendedResponse(200, $this->apiName." with ID $id updated");
                } else {
                    throw new \Exception('Fields missing', 500);
                }

            } else {
                throw new \Exception($this->apiName." not found with this id", 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }

    }

    public function deleteAction(Application $app, Request $request, $id)
    {
        try
        {
            $this->setApiName($request);
            if($object = $app['repo.'.$this->apiName]->findById($id))
            {
                $app['repo.'.$this->apiName]->delete($object);

                return $this->extendedResponse(200, $this->apiName.' with ID '. $id .' deleted');
            } else {
                throw new \Exception($this->apiName . ' not found', 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }
    }


    /******* UTILITY FUNCTIONS ***********/
    protected function setApiName(Request $request)
    {
        if(!$this->apiName) {
            //Récupération du nom de l'entité API à partir de la requête /api/{name} sans le s à la fin
            $this->apiName = rtrim(explode('/', $request->getPathInfo())[2], 's');
        }

        $this->transformer = 'Mkch\CoreApi\Transformer\\' . ucfirst($this->apiName) . 'Transformer';

        return $this;
    }

    protected function setIncludes(Request $request)
    {
        $this->includes = $request->get('include');

        return $this;
    }


    /**
    * Construct data response with fractal
    * @return array
    */
    protected function setCollection(Application $app, $objects, $returnType, $includes = null)
    {
        //Passer l'example dans le transformer
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());
        $returnType = "to" . $returnType;

        $resource = new Collection($objects, new GenericTransformer($app, $objects->first()));

        if($includes) {
            $fractal->parseIncludes($includes);
        }

        return $fractal->createData($resource)->{$returnType}();
    }

    /**
    * Construct data response with fractal
    * @return array
    */
    protected function setItem(Application $app, $object, $returnType, $includes = null)
    {
        //Passer l'example dans le transformer
        $fractal = new Manager();
        $fractal->setSerializer(new ArraySerializer());
        $returnType = "to" . $returnType;

        $resource = new Item($object, new GenericTransformer($app, $object));

        if($includes) {
            $fractal->parseIncludes($includes);
        }

        return $fractal->createData($resource)->{$returnType}();
    }


    /******* JSON RESPONSE FUNCTIONS ***********/
    public function extendedResponse($code, $status, $content = false)
    {
        if (!$content)
        {
            $response = array(
                'status' => $status,
                'code' => $code,
            );
        }
        elseif (isset($content['data'])) {
            $response = array(
                'status' => $status,
                'code' => $code,
                'data' => $content['data']
            );
        } else {
            $response = array(
                'status' => $status,
                'code' => $code,
                'data' => $content
            );
        }

        return new JsonResponse($response, $code);
    }

    public function extendedNotFoundResponse($e)
    {
        $response = array(
            'error' => array(
                'statusTxt' => $e->getMessage(),
                'status' => $e->getCode()
            ),
            'success' => false,
        );

        return new JsonResponse($response, Response::HTTP_NOT_FOUND);
    }
}
