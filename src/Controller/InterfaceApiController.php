<?php

// L'objectif de cette classe est de pouvoir passer l'Application aux contrôleurs, au lieu d'avoir à les passer en paramètre de chaque fonction

namespace Mkch\CoreApi\Controller;

use Mkch\CoreApi\Application;
use Symfony\Component\HttpFoundation\Request;

interface InterfaceApiController
{
    //Route get pour respecter la nomenclature API /api/{name}/{id}
    public function getAction(Application $app, Request $request, $id);

    //Route getAll pour respecter la nomenclature API /api/{name}
    public function getAllAction(Application $app, Request $request);

    public function createAction(Application $app, Request $request);

    public function updateAction(Application $app, Request $request, $id);

    public function deleteAction(Application $app, Request $request, $id);
}
