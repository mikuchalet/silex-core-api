<?php

namespace Mkch\CoreApi;

use Silex\Provider as SilexProvider;

use Mkch\CoreApi\Routing\RoutesLoader;
use Mkch\CoreApi\Factory\ApiFactory;

use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;


class Application extends \Silex\Application
{
    private $rootPath;
    private $appPath;
    private $srcPath;
    private $webPath;
    private $varPath;
    private $vendorPath;

    private $corePath = __DIR__;

    private $coreApiFactory;
    private $routesLoader;

    public function __construct()
    {
        parent::__construct();
        $this->initPaths();
        $this->setApplication();

    }

    public function initPaths()
    {
        //Case app with core in app
        if(substr(__DIR__, strripos(__DIR__, '/')) === '/core') {
            $this->rootPath = realpath(__DIR__ . '/..');
        }
        //Case app with packaged core
        else {
            //TODO : Mettre en package en voir quel chemin ressort (#flemme)
            $this->rootPath = __DIR__ . '';
        }

        $this->appPath = $this->rootPath . '/app';
        $this->srcPath = $this->rootPath . '/src';
        $this->webPath = $this->rootPath . '/web';
        $this->varPath = $this->rootPath . '/var';
        $this->vendorPath = $this->rootPath . '/vendor';
    }

    public function setApplication()
    {
        $this->registerConfig();
        $this->registerDatabase();
        $this->registerServices();
        $this->registerCore();
        $this->registerProviders();
        $this->registerCustomServices();
        $this->registerRoutes();

        if($this['vars']['params.api'])
        {
            $this->registerApi();
        }

        if($this['env'] === 'dev')
        {
            $this->registerDev();
        }

        return $this;
    }


    protected function registerCore()
    {
        $this->coreApiFactory = new ApiFactory($this);
    }

    protected function registerConfig()
    {
        // Load de Vars pour accès aux variables de config
        // https://github.com/m1/Vars
        // Accès aux variables par $this['vars']['ma.var']
        $this->register(new \M1\Vars\Provider\Silex\VarsServiceProvider($this->appPath . "/config/config.yml"), array(
            'vars.options' => array(
                'cache' => false,
                'cache_dir' => $this->appPath . "/cache",
                'merge_globals' => true
            )
        ));

        $this->setEnvironment();
    }

    protected function setEnvironment()
    {
        // Set debug mode
        $this['debug'] = $this['vars']['debug'];

        if($this['debug'] === true)
            $this['env'] = 'dev';
        else
            $this['env'] = 'prod';
    }

    protected function registerDatabase()
    {
        //Définit l'utilisation de Doctrine
        $this->register(new SilexProvider\DoctrineServiceProvider(), array(
            'db.options' => $this['vars']['db.options']
        ));
    }

    protected function registerRoutes()
    {
        $this->routesLoader = new RoutesLoader($this);
    }


    protected function registerDev()
    {
        // Register global error and exception handlers
        ErrorHandler::register();
        ExceptionHandler::register();

        // Load Twig only on dev because API don't need it, just DebugBar
        $this->registerTwig();

        $this->register(new SilexProvider\WebProfilerServiceProvider(), array(
            'profiler.cache_dir' => $this->getVarPath() . '/cache/profiler',
            'profiler.mount_prefix' => '/_profiler',
        ));
        $this->register(new SilexProvider\VarDumperServiceProvider());
    }


    protected function registerServices()
    {

        // Monolog
        $this->register(new SilexProvider\MonologServiceProvider(), array(
            'monolog.class_path'    => $this->vendorPath . '/monolog/src',
            'monolog.logfile'       => $this->varPath . '/log/api.log',
            'monolog.name'          => $this['vars']['monolog.name'],
            'monolog.level'         => $this['vars']['monolog.level']
        ));

        // Serializer / Déserializer les Models
        $this->register(new SilexProvider\SerializerServiceProvider());
    }


    protected function registerCustomServices()
    {
        // File Upload Service
        $this['file_uploader'] = function($app) {
            return new \Mkch\CoreApi\Modules\Files\Services\FileUploader($app);
        };

        // Auth Service
        $this['api_auth'] = function($app) {
            return new \Mkch\CoreApi\Modules\Auth\Services\ApiAuthenticator($app);
        };
    }


    protected function registerApi()
    {
        // Enable CORS for AJAX requests
        $this->register(new \JDesrosiers\Silex\Provider\CorsServiceProvider(), array(
            "cors.allowOrigin" => "http://localhost:8083",
            "cors.exposeHeaders" => $this['vars']['security.jwt']['options']['header_name']
        ));

        $this['cors-enabled']($this);
    }

    protected function registerTwig()
    {
        $this->register(new SilexProvider\TwigServiceProvider(), array(
            'twig.path' => [
                $this->appPath . '/views'
            ],
            'debug' => $this['vars']['twig.debug']
        ));

        $this['twig'] = $this->extend('twig', function(\Twig_Environment $twig, $app) {
            $twig->addExtension(new \Twig_Extensions_Extension_Text());
            $twig->addGlobal('app_dir', $this->appPath);
            $twig->addGlobal('root_dir', $this->rootPath);
            $twig->addGlobal('web_dir', $this->webPath);
            return $twig;
        });

        $this->error(function (\Exception $e, Request $request, $code) {
            return $this['twig']->render('errors/404.html.twig', array('message' => $e->getMessage(), 'code' => $code));
        });
    }

    protected function registerProviders()
    {
        $this->register(new SilexProvider\SessionServiceProvider());
        $this->register(new SilexProvider\SecurityServiceProvider(), array(
            'security.firewalls' => array(
                'secure_api' => array(
                    'pattern' => $this['vars']['security.firewalls.secure_api.pattern'],
                    'anonymous' => $this['vars']['security.firewalls.secure_api.anonymous'],
                    'users' => $this['users']
                )
            ),
            'security.role_hierarchy' => $this['vars']['security.role_hierarchy'],
            'security.access_rules' => $this['vars']['security.access_rules'],
            'security.jwt' => $this['vars']['security.jwt']
        ));

        $this->register(new SilexProvider\HttpFragmentServiceProvider());
        $this->register(new SilexProvider\ServiceControllerServiceProvider($this));
        $this->register(new SilexProvider\FormServiceProvider());
        $this->register(new SilexProvider\LocaleServiceProvider());
        $this->register(new SilexProvider\TranslationServiceProvider());
        $this->register(new SilexProvider\AssetServiceProvider(), array(
            'assets.version' => 'v1'
        ));
        $this->register(new SilexProvider\ValidatorServiceProvider());

    }

    // Helper function for recursive require directory
    protected function __autoload($dir) {

        // require all php files
        $scan = glob("$dir/*");
        foreach ($scan as $path) {
            if (preg_match('/\.php$/', $path)) {
                require_once $path;
            }
            elseif (is_dir($path)) {
                $this->__autoload($path);
            }
        }
    }


    public function getWebPath()
    {
        return $this->webPath;
    }


    public function setWebPath($webPath)
    {
        $this->webPath = $webPath;
        return $this;
    }


    public function getVarPath()
    {
        return $this->varPath;
    }


    public function setVarPath($varPath)
    {
        $this->varPath = $varPath;
        return $this;
    }

    public function getSrcPath()
    {
        return $this->srcPath;
    }

    public function setSrcPath($srcPath)
    {
        $this->srcPath = $srcPath;
        return $this;
    }

    public function getAppPath()
    {
        return $this->appPath;
    }

    public function setAppPath($appPath)
    {
        $this->appPath = $appPath;
        return $this;
    }


    public function getRootPath()
    {
        return $this->rootPath;
    }

    public function setRootPath($rootPath)
    {
        $this->rootPath = $rootPath;
        return $this;
    }


    public function getApiRoutesLoader()
    {
        return $this->apiRoutesLoader;
    }

    public function setApiRoutesLoader($apiRoutesLoader)
    {
        $this->apiRoutesLoader = $apiRoutesLoader;

        return $this;
    }


    public function getActionsRoutesLoader()
    {
        return $this->actionsRoutesLoader;
    }

    public function setActionsRoutesLoader($actionsRoutesLoader)
    {
        $this->actionsRoutesLoader = $actionsRoutesLoader;

        return $this;
    }

    public function getCorePath()
    {
        return $this->corePath;
    }

    public function setCorePath($corePath)
    {
        $this->corePath = $corePath;

        return $this;
    }

    public function getRoutesLoader()
    {
        return $this->routesLoader;
    }

    public function setRoutesLoader($routesLoader)
    {
        $this->routesLoader = $routesLoader;

        return $this;
    }

    public function getCoreApiFactory()
    {
        return $this->coreApiFactory;
    }

    public function setCoreApiFactory($coreApiFactory)
    {
        $this->coreApiFactory = $coreApiFactory;

        return $this;
    }

}
