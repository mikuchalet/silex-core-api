<?php

namespace Mkch\CoreApi\Repository;

interface InterfaceRepository
{
    public function findAll(array $orderBy = null);
    public function findByParams(array $params, array $orderBy = null, $limit = null, $offset = null);

    public function delete($id);
    public function save($object);

    public function buildModelObject(array $array, $object = null);
    public function objectToArray($object);

}
