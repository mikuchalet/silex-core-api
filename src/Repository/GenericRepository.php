<?php

namespace Mkch\CoreApi\Repository;

use Mkch\CoreApi\Application;

use Mkch\CoreApi\Model\GenericModel;
use Doctrine\DBAL\Connection;

use Doctrine\Common\Collections\ArrayCollection;

//Classe permettant l'accès aux données de la BDD
//Work with GenericModel
class GenericRepository implements InterfaceRepository
{
    protected $db;
    protected $app;
    protected $tableName;
    protected $model = "Mkch\CoreApi\Model\GenericModel";

    public function __construct(Connection $db, Application $app, $tableName) {
        $this->db = $db;
        $this->app = $app;
        $this->tableName = $tableName;
    }

    /**
    *   @param integer id
    *   @return GenericModel|boolean
    */
    public function findById($id)
    {
        $qb = $this->db->createQueryBuilder()
            ->select('*')
            ->from($this->tableName)
            ->where('id = :id')
            ->setParameter('id', $id);

        if($result = $qb->execute()->fetch()) {
            return $this->buildModelObject($result, new $this->model($this->tableName, $this->app['model.'.$this->tableName]));
        }

        return false;

    }


    /**
    *
    *   @return ArrayCollection of GenericModel|boolean
    */
    public function findAll(array $orderBy = null)
    {
        $objects = new ArrayCollection();

        $qb = $this->db->createQueryBuilder()
            ->select('*')
            ->from($this->tableName);



        if($orderBy !== NULL) {
            $qb->orderBy(key($orderBy[0]), $orderBy[0]);
        }

        if($results = $qb->execute()->fetchAll()) {
            foreach($results as $result) {
                $object = $this->buildModelObject($result, new GenericModel($this->tableName, $this->app['model.'.$this->tableName]));
                $objects->add($object);
            }

            return $objects;
        }

        return false;


    }


    /**
    *
    *   @return ArrayCollection of GenericModel|boolean
    */
    public function findByParams(array $params, array $orderBy = null, $limit = null, $offset = null)
    {
        $qb = $this->db->createQueryBuilder()
            ->select('*')
            ->from($this->tableName);

        foreach($params as $param => $value) {
            $qb->andWhere("$param = :p_$param")
                ->setParameter("p_$param", $value);
        }

        if($orderBy !== NULL) {
            $qb->orderBy(key($orderBy[0]), $orderBy[0]);
        }

        if($offset !== NULL) {
            $qb->setFirstResult($offset);
        }

        if($limit !== NULL) {
            $qb->setMaxResults($limit);

            if($limit === 1) {
                if($result = $qb->execute()->fetch()) {
                    return $result;
                } else {
                    return false;
                }
            }
        }

        if($results = $qb->execute()->fetchAll()) {
            foreach($results as $result) {
                $object = $this->buildModelObject($result, new GenericModel($this->tableName, $this->app['model.'.$this->tableName]));
                $objects->add($object);
            }

            return $objects;
        }

        return false;

    }

    /**
    *   @param GenericModel
    *   @return boolean
    */
    public function delete($object)
    {
        return $this->db->delete($this->tableName, array('id' => $object->__get('id')));
    }

    /**
    *   @param GenericModel
    *   @return GenericModel
    */
    public function save($object)
    {
        $array = $this->objectToArray($object);

        if($object->__get('id')) {

            if($this->getDb()->update($this->tableName, $array, array('id' => $object->__get('id')))) {
                return $object;
            }
        } else {

            if($this->db->insert($this->tableName, $array)) {
                $object->__set('id', $this->db->lastInsertId());
                return $object;
            }
        }
    }

    /**
    *   @param array
    *   @return GenericModel
    */
    public function buildModelObject(array $array, $object = null)
    {
        if($object) {
            if($object instanceof GenericModel) {
                foreach($array as $property => $value) {
                    $object->__set($property, $value);
                }
            }
            return $object;
        } else {
            return $this->getApp()['serializer']->denormalize($array, $this->model);
        }

    }

    /**
    *   @param GenericModel
    *   @return array
    */
    public function objectToArray($object)
    {
        $array = array();
        foreach($object->getProperties() as $property => $type) {

            //Related Model case : rajoute "_id" à la propriété
            if($type['format'] === 'Model') {
                $field = $property . '_id';
                $array[$field] = $object->__get($property);
            } else {
                $array[$property] = $object->__get($property);
            }
        }
        return $array;

        // Serializer not working on GenericModel
        // return $this->getApp()['serializer']->normalize($object);
    }

    protected function getDb() {
        return $this->db;
    }

    protected function getApp() {
        return $this->app;
    }

    protected function getTableName()
    {
        return $this->tableName;
    }


    protected function getModel()
    {
        return $this->model;
    }

}
