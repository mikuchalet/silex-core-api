<?php

namespace Mkch\CoreApi\Helper;

class RecursiveFilesHelper
{

    public static function getFilePathsByDirectory($dir, $extension = null, $arrayFiles = array())
    {
        // Scan files in $dir
        // http://php.net/manual/en/class.recursivedirectoryiterator.php
        $scan = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator("$dir"));

        foreach ($scan as $path) {
            if(!$extension) {
                if (!is_dir($path)) {
                    array_push($arrayFiles, $path->getPathName());
                }
            } else {
                if (!is_dir($path) && preg_match("/\.$extension$/", $path)) {
                    array_push($arrayFiles, $path->getPathName());
                }
            }
        }

        return $arrayFiles;

    }

    // NOT WORKING
    // Charge les fichiers contenus dans le dossier
    // Utilisé dans Application pour load les fichiers de route, mais perte de contexte
    // public static function loadByDirectory($dir, $extension = null)
    // {
    //     // require all php files
    //     $scan = glob("$dir/*");
    //
    //     foreach ($scan as $path) {
    //         if(!$extension) {
    //             if (is_dir($path)) {
    //                 self::loadByDirectory($path, $extension);
    //             }
    //             else {
    //                 require_once $path;
    //             }
    //         } else {
    //             if (preg_match("/\.$extension$/", $path)) {
    //                 require_once $path;
    //             }
    //             elseif (is_dir($path)) {
    //                 self::loadByDirectory($path, $extension);
    //             }
    //         }
    //     }
    // }
}
