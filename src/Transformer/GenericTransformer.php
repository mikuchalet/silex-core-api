<?php

namespace Mkch\CoreApi\Transformer;

use League\Fractal\TransformerAbstract;
use League\Fractal\Scope;
use League\Fractal\Resource\ResourceInterface;

use Mkch\CoreApi\Model\GenericModel;
use Mkch\CoreApi\Application;


/**
*   Class using Fractal : https://github.com/thephpleague/fractal
*   Works with GenericModel
*/
class GenericTransformer extends TransformerAbstract
{
    protected $app;
    protected $model;
    protected $properties;


    public function __construct(Application $app, GenericModel $model)
    {
        $this->app = $app;
        $this->model = $model;
        $this->properties = $model->getProperties();

        //Set availableIncludes
        //Récupérer les propriétés de type "relational"
        foreach($this->properties as $name => $params) {
            if($params['format'] === "Model") {
                if($params['cascade'] === true) {
                    array_push($this->availableIncludes, $name);
                }
                if($params['cascade'] === 'default') {
                    array_push($this->defaultIncludes, $name);
                }
            }
        }
    }

    /**
    *   Transform object to an Array of GenericModel properties
    */
    public function transform($object)
    {
        //Set PrimaryKey
        $array[key($object->getPrimaryKey())] = $object->__get(key($object->getPrimaryKey()));

        //Set other properties
        foreach($this->properties as $name => $params) {
            $array[$name] = $object->__get($name);
        }

        return $array;
    }


    /**
    *   New "include" generic function
    *   Search all fields which have "format" => "Model" in GenericModel
    */
    public function includeRelated(GenericModel $object, $related)
    {

        $relatedId = $object->__get($related);

        //Appel repo pour récupérer l'objet
        $related = $this->app['repo.' . $related]->findById($relatedId);

        return $this->item($related, new self($this->app, $related));
    }

    /**
     * Rewrite callIncludeMethod for being Generic
     * Call Include Method.
     *
     * @internal
     *
     * @param Scope  $scope
     * @param string $includeName
     * @param mixed  $data
     *
     * @throws \Exception
     *
     * @return \League\Fractal\Resource\ResourceInterface
     */
    protected function callIncludeMethod(Scope $scope, $includeName, $data)
    {
        //The type of related asked by Request
        $scopeIdentifier = $scope->getIdentifier($includeName);

        //Other params
        $params = $scope->getManager()->getIncludeParams($scopeIdentifier);

        $resource = $this->includeRelated($data, $scopeIdentifier);

        if ($resource === null) {
            return false;
        }

        if (! $resource instanceof ResourceInterface) {
            throw new \Exception(sprintf(
                'Invalid return value from %s::%s(). Expected %s, received %s.',
                __CLASS__,
                $methodName,
                'League\Fractal\Resource\ResourceInterface',
                is_object($resource) ? get_class($resource) : gettype($resource)
            ));
        }

        return $resource;
    }
}
