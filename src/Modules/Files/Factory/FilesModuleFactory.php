<?php

namespace Mkch\CoreApi\Modules\Files\Factory;

use Mkch\CoreApi\Application;

use Mkch\CoreApi\Factory\InterfaceApiFactory;
use Mkch\CoreApi\Repository\GenericRepository;

use Mkch\CoreApi\Helper\RecursiveFilesHelper;

use Symfony\Component\Yaml\Yaml;

class FilesModuleFactory implements InterfaceApiFactory
{
    private $app;
    private $modelsPath;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->modelsPath = __DIR__ . '/../conf/models';

        $models = $this->loadYmlModels();

        foreach($models as $model)
        {
            //Parsing
            $arrayModel = Yaml::parse(file_get_contents($model));

            //Création model/repo/api_routes
            $this->registerModel($arrayModel);
            $this->registerRepository(key($arrayModel));
        }
    }

    public function loadYmlModels()
    {
        // Récupérer chaque fichier dans le dossier app/modules/models
        return RecursiveFilesHelper::getFilePathsByDirectory($this->modelsPath, 'yml');
    }

    public function loadJsonModels()
    {
        // Récupérer chaque fichier dans le dossier routes
        return RecursiveFilesHelper::getFilePathsByDirectory($this->modelsPath, 'json');
    }

    public function registerRepository($name)
    {
        //$app['repo.user']
        $this->app['repo.' . $name] = function ($app) use ($name) {
            return new GenericRepository($app['db'], $app, $name);
        };

    }

    public function registerModel($arrayModel)
    {
        //Ajout de la structure de données dans l'app (pour pouvoir la récupérer et vérifier côté controller)
        return $this->app['model.'.key($arrayModel)] = $arrayModel[key($arrayModel)];
    }
}
