<?php


namespace Mkch\CoreApi\Modules\Files\Services;


use Mkch\CoreApi\Application;

use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploader
{


    private $app;

    private $path;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->path = $app['vars']['web_path'] . '/uploads/';
        $this->displayPath = '/uploads/';
    }

    public function getApp()
    {
        return $this->app;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getDisplayPath()
    {
        return $this->displayPath;
    }

    /**
    *   Prend une instance d'UploadFile, génère un nom unique pour éviter les doublons et renvoie une instance de File
    *   @param Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile
    *   @param string $customPath
    *   @return Symfony\Component\HttpFoundation\File\File $file
    */
    public function uploadFile($uploadedFile, $customPath = null)
    {
        try {
            // Check if customPath is defined, else use $this->path
            (isset($customPath) && !empty($customPath)) ? $path = $customPath : $path = $this->path;

            // Filename = Nom du fichier original + l'inode (pour ID unique) + l'extension
            $uniqueName = substr($uploadedFile->getClientOriginalName(), 0, strpos($uploadedFile->getClientOriginalName(), '.'))
                . $uploadedFile->getInode()
                . '.'
                . $uploadedFile->getClientOriginalExtension();

            if($file = $uploadedFile->move($path, $uniqueName)) {
                return $file;
            } else {
                throw new UploadException('Upload fail', 500);
            }
        }
        catch (UploadException $e) {
            throw $e;
        }
    }

    /**
    *   Remove a file by path
    *   @param string $filePath
    *   @return boolean
    */
    public function removeFile($filePath)
    {
        try {
            if(unlink($this->path . $filePath)) {
                return true;
            } else {
                throw new FileException('Fail removing file', 500);
            }
        }
        catch (FileException $e) {
            throw $e;
        }
    }

    public function renameFile($old, $new)
    {
        try {
            if(rename($this->path . $old, $this->path . $new)) {
                return true;
            } else {
                throw new FileException('Fail updating filename', 500);
            }
        }
        catch (FileException $e) {
            throw $e;
        }
    }
}
