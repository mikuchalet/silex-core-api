<?php

namespace Mkch\CoreApi\Modules\Files\Controller;

use Mkch\CoreApi\Application;

use Mkch\CoreApi\Controller\GenericApiController;
use Mkch\CoreApi\Model\GenericModel;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class FileController extends GenericApiController
{

    public function __construct()
    {
        $this->apiName = "file";
    }

    public function createAction(Application $app, Request $request)
    {
        try {

            $files = $request->files;

            foreach($files as $file) {

                // Mkch\CoreApi\Modules\Files\Services\FileUploader
                $tmp = $app['file_uploader']->uploadFile($file);

                // GenericModel
                $object = new GenericModel($this->apiName, $app['model.' . $this->apiName]);
                $object->__set('size', $tmp->getSize());
                $object->__set('type', $tmp->getMimeType());
                $object->__set('path', $tmp->getFileName());

                if($object->verifyModel() === true) {
                    $app['repo.'.$this->apiName]->save($object);

                    return $this->extendedResponse(200, $this->apiName." created");
                } else {
                    throw new \Exception("Wrong parameters", 500);
                }

                $this->getApp()['repo.file']->save($object);

                return new JsonResponse('Votre fichier a bien été importé', 200);
            }
        }
        catch (UploadException $e) {
            return $this->extendedNotFoundResponse($e);
        }
        catch (\Exception $e) {
            return $this->extendedNotFoundResponse($e);
        }
    }

    public function deleteAction(Application $app, Request $request, $id)
    {
        try
        {
            if($object = $app['repo.'.$this->apiName]->findById($id))
            {
                $app['file_uploader']->removeFile($object->__get('path'));

                $app['repo.'.$this->apiName]->delete($object);

                return $this->extendedResponse(200, $this->apiName.' with ID '. $id .' deleted');
            } else {
                throw new \Exception($this->apiName . ' not found', 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }
    }

    public function updateAction(Application $app, Request $request, $id)
    {
        try
        {
            if($object = $app['repo.'.$this->apiName]->findById($id))
            {
                // Parsing de la request
                foreach($request->query->all() as $param => $value) {
                    //Si modification du path, on modifie le fichier
                    if($param === 'path' && $value !== $object->__get('path')) {
                        $app['file_uploader']->renameFile($object->__get('path'), $value);
                    }

                    $object->__set($param, $value);
                }

                if($object->verifyModel() === true) {

                    $app['repo.'.$this->apiName]->save($object);

                    return $this->extendedResponse(200, $this->apiName." with ID $id updated");
                } else {
                    throw new \Exception('Fields missing', 500);
                }

            } else {
                throw new \Exception($this->apiName." not found with this id", 404);
            }
        }
        catch (\Exception $e)
        {
            return $this->extendedNotFoundResponse($e);
        }

    }
}
