<?php

namespace Mkch\CoreApi\Modules\Auth\Controller;

use Mkch\CoreApi\Application;
use Mkch\CoreApi\Controller\GenericApiController;
use Mkch\CoreApi\Model\GenericModel;
use Mkch\CoreApi\Modules\Auth\Token\JsonWebToken;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;

class AuthController extends GenericApiController
{
    public function __construct()
    {
        $this->apiName = "user";

    }
    /** POST - Authenticate User **/
    public function loginAction(Application $app, Request $request)
    {
        $data = array();

        try {

            if($request->get('username') && $request->get('password')) {
                $data['username'] = $request->get('username');
                $data['password'] = $request->get('password');
            } else {
                throw new BadCredentialsException(sprintf('Username "%s" not found', $request->get('username')), 500);
            }


            // Check if a JWT is stored in request headers
            if($token = $request->headers->get($app['security.jwt']['options']['header_name'])) {

                // If token is not blacklisted, check if not expired and return it, or regenerate it
                if(!$token->isBlacklisted()) {

                    if(!$token->isExpired()) {
                        $response = array(
                            'status' => 'not expired',
                            'success' => true,
                            'code' => 200
                        );
                    } else {

                        // TODO
                        die('Token expiré');

                    }


                } else {
                    throw new AuthenticationException('Invalid token sent', 500);
                }
            }
            // If no token, check credentials generate it
            else {
                if($user = $app['api_auth']->validateCredentials($data)) {

                    $jwt = $app['api_auth']->generateUserToken($user);

                    $response = array(
                        'status' => 'new token',
                        'success' => true,
                        'code' => 200
                    );

                } else {
                    throw new BadCredentialsException(sprintf('Username "%s" not found', $data['username']), 500);
                }
            }

            return new JsonResponse($response, 200, array(
                $app['security.jwt']['options']['header_name'] => $jwt->getToken()->__toString()
            ));

        } catch (BadCredentialsException $e) {
            $response = array(
                'error' => array(
                    'msg' => $e->getMessage(),
                    'code' => $e->getCode()
                ),
                'success' => false,
            );

            return $app->json($response, Response::HTTP_UNAUTHORIZED);

        } catch (AuthenticationException $e) {
            $response = array(
                'error' => array(
                    'msg' => $e->getMessage(),
                    'code' => $e->getCode()
                ),
                'success' => false,
            );

            return $app->json($response, Response::HTTP_UNAUTHORIZED);

        }

    }

    /** POST - Register new user **/
    public function createAction(Application $app, Request $request)
    {
        $data = array();

        try {

            if($request->get('email') && $request->get('password') && $request->get('password_confirm')) {

                // Check if password confirmation is ok
                if($request->get('password') !== $request->get('password_confirm')) {
                    throw new BadCredentialsException('Make sure you confirm the password');
                }

                // Generic case if you don't want username authentication
                if($request->get('username') === null) {
                    $data['username'] = $request->get('email');
                } else {
                    $data['username'] = $request->get('username');
                }

                $data['email'] = $request->get('email');
                $data['password'] = $request->get('password');

                // Check if email is not registered yet
                if($app['repo.user']->findByEmail($data['email'])) {
                    throw new AuthenticationException(sprintf('Email %s is already used', $data['email']));
                }

                // Check if username is not registered yet
                if($app['repo.user']->loadUserByUsername($data['username'])) {
                    throw new AuthenticationException(sprintf('Username %s is already used', $data['username']));
                }

                // If all OK, create User
                $user = new GenericModel($this->apiName, $app['model.' . $this->apiName]);

                // generate a random salt value
                $salt = substr(md5(time()), 0, 23);
                $user->__set('salt', $salt);
                $user->__set('username', $data['username']);
                $user->__set('email', $data['email']);
                $user->__set('role', $app['security.jwt']['options']['default_role']);
                $user->__set('is_active', false);


                $encoder = $app['security.default_encoder'];
                $user->__set('password', $encoder->encodePassword($data['password'], $user->__get('salt')));

                $app['repo.user']->save($user);

                $response = array(
                    'status' => 'Registration complete, need confirmation',
                    'success' => true,
                    'code' => 200
                );

                return new JsonResponse($response, 200);

            } else {
                throw new InvalidParameterException(sprintf('Please resend the request with appropriate params'), 500);
            }

        } catch (InvalidParameterException $e) {

            $response = array(
                'error' => array(
                    'msg' => $e->getMessage(),
                    'code' => $e->getCode()
                ),
                'success' => false,
            );

            return $app->json($response, Response::HTTP_UNAUTHORIZED);

        } catch (AuthenticationException $e) {
            $response = array(
                'error' => array(
                    'msg' => $e->getMessage(),
                    'code' => $e->getCode()
                ),
                'success' => false,
            );

            return $app->json($response, Response::HTTP_UNAUTHORIZED);
        }

    }

}
