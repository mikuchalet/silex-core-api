<?php

namespace Mkch\CoreApi\Modules\Auth\Repository;

use Mkch\CoreApi as CoreApi;

use Mkch\CoreApi\Repository\GenericRepository;
use Mkch\CoreApi\Application;
use Doctrine\DBAL\Connection;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
*   Custom Repository class for using with Silex Security
*/
class UserRepository extends GenericRepository implements UserProviderInterface
{

    public function loadUserByUsername($username)
    {
        $qb = $this->db->createQueryBuilder()
                    ->select('*')
                    ->from($this->getTableName())
                    ->where('username = :username')
                    ->setParameter('username', $username);

        if($result = $qb->execute()->fetch()) {
            return $this->buildModelObject($result, new $this->model($this->tableName, $this->app['model.'.$this->tableName]));
        } else {
            return false;
        }

    }


    public function findByEmail($email)
    {
        $qb = $this->db->createQueryBuilder()
                    ->select('*')
                    ->from($this->getTableName())
                    ->where('email = :email')
                    ->setParameter('email', $email);

        if($result = $qb->execute()->fetch()) {
            return $this->buildModelObject($result, new $this->model($this->tableName, $this->app['model.'.$this->tableName]));
        } else {
            return false;
        }
    }


    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }


    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return 'Portfolio\Model\User' === $class;
    }

}
