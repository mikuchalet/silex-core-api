<?php

namespace Mkch\CoreApi\Modules\Auth\Services;

use Mkch\CoreApi\Application;
use Mkch\CoreApi\Modules\Auth\Token\JsonWebToken;
use Mkch\CoreApi\Model\GenericModel;

use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;


class ApiAuthenticator
{

    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
    *   Retrieve User with credentials
    */
    public function getUser($username)
    {
        if($user = $this->app['repo.user']->loadUserByUsername($username)) {
            return $user;
        } else {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }
    }

    /**
    *   Check if credentials are ok
    */
    public function validateCredentials($data)
    {
        $user = $this->getUser($data['username']);

        // Petite fonction php qui va bien pour comparer le password
        if(password_verify($data['password'], $user->__get('password'))) {
            return $user;
        } else {
            throw new BadCredentialsException('Bad Credentials');
        }
    }

    /**
    *   Check if token is ok
    */
    public function validateToken(User $user)
    {
        // Check date d'expiration

        // Si expiré, regénération d'un token

        // Si blacklisté, renvoie une exception

        // Sinon, renvoi du token
    }


    /**
    *   Generate New Token
    */
    public function generateUserToken(GenericModel $user)
    {

        $claims = array(
            //'email' => $user->getEmail(),
            'username' => $user->__get('username'),
            'role' => $user->__get('role'),
            'email' => $user->__get('email'),
            'email_verified' => $user->__get('is_active'),
        );

        $token = new JsonWebToken($this->app, $claims);


        //dump($token->getToken()->__toString('base64'));die;
        // Set user infos in claims


        return $token;
    }
}
