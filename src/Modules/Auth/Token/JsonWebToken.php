<?php

namespace Mkch\CoreApi\Modules\Auth\Token;

use Mkch\CoreApi\Application;
use Mkch\CoreApi\Modules\Auth\Model\User;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;


class JsonWebToken
{
    private $token;
    private $claims;
    private $headers;
    private $lifetime;
    private $algo = null;
    private $secret = null;

    public function __construct(Application $app, $claims = null)
    {
        $this->claims = array();
        $this->headers = array();

        // Récupération des données contenues dans conf/security.yml
        if(isset($app['security.jwt']['algo']) && !empty($app['security.jwt']['algo'])
            && isset($app['security.jwt']['secret_key']) && !empty($app['security.jwt']['secret_key'])
            && isset($app['security.jwt']['life_time']) && !empty($app['security.jwt']['life_time'])
        ) {

            // Création des headers
            $this->algo = $app['security.jwt']['algo'];
            $this->secret = $app['security.jwt']['secret_key'];
            $this->lifetime = $app['security.jwt']['life_time'];
        }

        if($claims) {
            $this->claims = $claims;
        }

        $this->createToken();
    }

    /**
    *   Créer un token
    */
    public function createToken()
    {
        $builder = new Builder();

        // Id generation
        $builder->setId('1234');

        // Création des claims
        foreach($this->claims as $claim => $v) {
            $builder->set($claim, $v);
        }

        // Gestion de l'expiration
        $builder->setIssuedAt(time());

        if(isset($this->lifetime) && !empty($this->lifetime)) {
            $builder->setExpiration($this->lifetime);
        }

        // Si un algo est spécifié ainsi qu'une clé, encode le Token
        if(isset($this->algo) && !empty($this->algo)
            && isset($this->secret) && !empty($this->secret)
        ) {
            $builder = $this->encodeToken($builder, $this->algo, $this->secret);
        }

        $this->setToken($builder->getToken());

        return $this;
    }

    /**
    *   Si le token est expiré, regénère un token
    */
    public function regenerateToken()
    {

    }

    /**
    *   Encode le token
    */
    public function encodeToken($builder)
    {
        if($this->algo === "HS256") {
            $signer = new Sha256();
            $builder->sign($signer, $this->secret);
        }

        // TODO : Other cases for other signers

        return $builder;
    }


    /**
    *   Décode le token
    */
    public function decodeToken($builder)
    {

    }


    /**
    *   Teste si le token est expiré
    */
    public function isExpired()
    {

    }


    /**
    *   Teste si le token est blacklisté
    */
    public function isBlacklisted()
    {

    }


    /*** GETTERS / SETTERS ***/


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function getClaims()
    {
        return $this->claims;
    }

    public function setClaims($claims)
    {
        $this->claims = $claims;

        return $this;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    public function getAlgo()
    {
        return $this->algo;
    }

    public function setAlgo($algo)
    {
        $this->algo = $algo;

        return $this;
    }

    public function getSecret()
    {
        return $this->secret;
    }

    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

}
