<?php

namespace Mkch\CoreApi\Routing;

use Mkch\CoreApi\Application;
use Mkch\CoreApi\Routing\Route;
use Mkch\CoreApi\Routing\ApiRoute;
use Mkch\CoreApi\Helper\RecursiveFilesHelper;

use Symfony\Component\Yaml\Yaml;

use Doctrine\Common\Collections\ArrayCollection;

class RoutesLoader
{

    private $app;
    private $routesPath;
    private $routes;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->routesPath = $this->app->getAppPath() . '/routes';
        $this->routes = new ArrayCollection();

        $this->loadYmlFiles();

        //$this->loadJsonFiles();
    }

    public function loadYmlFiles()
    {
        $array = array();

        // Récupérer chaque fichier dans le dossier routes
        $files = RecursiveFilesHelper::getFilePathsByDirectory($this->getRoutesPath(), 'yml');

        // Add modules Routes
        foreach($this->app->getCoreApiFactory()->getModulesFactory()->getRoutesPath() as $path) {
            $modulesFiles = RecursiveFilesHelper::getFilePathsByDirectory($path, 'yml');

            if($modulesFiles) {
                foreach($modulesFiles as $file) {
                    array_push($files, $file);
                }
            }
        }

        foreach($files as $file)
        {
            if($routesParams = Yaml::parse(file_get_contents($file))) {
                $this->registerRoutes($routesParams);
            }
        }

        return;
    }

    public function loadJsonFiles()
    {
        // Récupérer chaque fichier dans le dossier routes
        $files = RecursiveFilesHelper::getFilePathsByDirectory($this->getRoutesPath(), 'json');

        foreach($files as $file)
        {
            $arrayRoutes = json_decode(file_get_contents($file), true);
            dump($arrayRoutes);

        }

        return;
    }


    public function registerRoutes($routesParams)
    {

        try {

            // Check required params
            if(!array_key_exists('name', $routesParams) || $routesParams['name'] === null) {
                throw new \Exception('You need to set a "name" attribute');
            }

            if(!array_key_exists('controller', $routesParams) || $routesParams['controller'] === null) {
                throw new \Exception('You need to set the controller namespace as "controller" attribute');
            }

            if(!array_key_exists('api_routes', $routesParams) || $routesParams['api_routes'] === null) {
                throw new \Exception('You need to set a "api_routes" [bool] attribute');
            }


            // Get global params
            $globalName = $routesParams['name'];
            $controller = $routesParams['controller'];

            $baseUrl = "";

            // Construct base url
            if($routesParams['api_routes'] === true) {
                $baseUrl = "/api";
            }

            if(array_key_exists('base_url', $routesParams) || !empty($routesParams['base_url'])) {
                $baseUrl .= $routesParams['base_url'];
            }
            else {
                $baseUrl .= '/'.$globalName.'s';
            }

            // Register API routes : create, read, update, delete
            if($routesParams['api_routes'] === true) {
                if(array_key_exists('api_options', $routesParams) || !empty($routesParams['api_options'])) {
                    $apiOptions = $routesParams['api_options'];
                } else {
                    $apiOptions = null;
                }

                $this->registerApiRoutes($globalName, $controller, $baseUrl, $apiOptions);
            }

            if(!empty($routesParams['actions'])) {

                // Register all routes in "actions"
                foreach($routesParams['actions'] as $name => $options) {


                    $route = new Route(
                        $this->getApp(),
                        $baseUrl,
                        $globalName,
                        $controller,
                        $name,
                        $options
                    );

                    $this->addRoute($route);
                }
            }
            // dump($this);
        }
        catch(\Exception $e) {
            //die($e->getMessage());
        }
    }

    public function registerApiRoutes($globalName, $controller, $baseUrl, $apiOptions)
    {
        // Prepare options for REST methods
        $apiMethods = array(
            'get' => array(
                'methods' => array('GET')
            ),
            'getAll' => array(
                'methods' => array('GET')
            ),
            'create' => array(
                'methods' => array('POST')
            ),
            'update' => array(
                'methods' => array('PUT')
            ),
            'delete' => array(
                'methods' => array('DELETE')
            )
        );

        if($apiOptions !== NULL) {

            // Récupération des rôles pour chaque route
            foreach($apiOptions['roles'] as $value) {
                $methods[key($value)]['roles'] = $value[key($value)];
            }

            // TODO : Ajouter d'autres boucles si besoin d'autres options

        }

        foreach($apiMethods as $name => $options) {

            // Add ID parameter as url for get, delete, update
            if(in_array($name, array('get', 'delete', 'update'))) {
                $options['url'] = '/{id}';

                // Forcer le passage d'un entier
                $options['requirements'] = array(
                    'id' => '\d+'
                );
            }

            $route = new Route(
                $this->getApp(),
                $baseUrl,
                $globalName,
                $controller,
                $name,
                $options
            );

            $this->addRoute($route);
        }



    }

    // Getters & Setters


    public function getApp()
    {
        return $this->app;
    }

    public function setApp($app)
    {
        $this->app = $app;

        return $this;
    }


    public function getRoutesPath()
    {
        return $this->routesPath;
    }

    public function setRoutesPath($routesPath)
    {
        $this->routesPath = $routesPath;

        return $this;
    }


    public function getRoutes()
    {
        return $this->routes;
    }

    public function addRoute(Route $route)
    {
        $this->routes->add($route);

        return $this;
    }

    public function setRoutes(Route $routes)
    {
        $this->routes = $routes;

        return $this;
    }

}
