<?php

namespace Mkch\CoreApi\Routing;

use Mkch\CoreApi\Application;

use Symfony\Component\Security\Core\Exception as BaseException;

class Route
{
    // Use trait for secure route
    //use \Silex\Route\SecurityTrait;

    private $app;

    private $baseUrl;
    private $globalName;
    private $controller;
    private $name;
    private $options;

    public function __construct(Application $app, $baseUrl, $globalName, $controller, $name = "", $options = array())
    {
        $this->app = $app;
        $this->baseUrl = $baseUrl;
        $this->globalName = $globalName;
        $this->controller = $controller;
        $this->options = $options;
        $this->name = $name;

        $this->load();
    }

    public function load()
    {
        // URL construction
        if(!empty($this->getBaseUrl())) {
            $url = $this->getBaseUrl();
        } else {
            $url = $this->getName();
        }

        if(isset($this->getOptions()['url'])) {
            $url .= $this->getOptions()['url'];
        }


        // Set full URL
        $this->addOption('url', $url);

        // Construction of route
        $routeInstance = $this->getApp()->match($url, $this->getController() . '::' . $this->getName() . 'Action');

        if(!empty($this->getOptions()['bind_to'])) {
            $routeInstance->bind($this->getOptions()['bind_to']);
        }

        if(!empty($this->getOptions()['methods'])) {
            $routeInstance->method(implode($this->getOptions()['methods']));
        }

        if(!empty($this->getOptions()['requirements'])) {
            foreach($this->getOptions()['requirements'] as $param => $regex) {
                $routeInstance->assert($param, $regex);
            }
        }

        if(!empty($this->getOptions()['roles'])) {
            $roles = implode($this->getOptions()['roles'], ',');

            // Redeclare function in SecurityTrait because can't use it (wrong instance)
            $routeInstance->before(function ($request, $app) use ($roles) {
                if (!$app['security.authorization_checker']->isGranted($roles)) {
                    throw new BaseException\AccessDeniedException();
                }
            });

        }

        return $this;
    }


    // Getters & Setters


    public function getApp()
    {
        return $this->app;
    }

    public function setApp($app)
    {
        $this->app = $app;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function setController($controller)
    {
        $this->controller = $controller;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    public function addOption($key, $value)
    {
        $this->options[$key] = $value;

        return $this;
    }

    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getGlobalName()
    {
        return $this->globalName;
    }

    public function setGlobalName($globalName)
    {
        $this->globalName = $globalName;

        return $this;
    }

}
