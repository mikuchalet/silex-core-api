<?php

namespace Mkch\CoreApi\Factory;

use Mkch\CoreApi\Application;
use Mkch\CoreApi\Model\GenericModel;
use Mkch\CoreApi\Repository\GenericRepository;
use Mkch\CoreApi\Factory\ModulesFactory;
use Mkch\CoreApi\Helper\RecursiveFilesHelper;

use Symfony\Component\Yaml\Yaml;


class ApiFactory implements InterfaceApiFactory
{
    protected $app;
    private $modelsPath;
    private $modulesFactory;

    public function __construct(Application $app)
    {
        $this->app = $app;

        /*** Register core modules models ***/
        $this->modulesFactory = new ModulesFactory($app);
        $this->loadModules();


        /*** Register app models ***/
        $this->modelsPath = $this->app->getAppPath() . '/modules/models';

        //Récupérer les models.yml
        $models = $this->loadYmlModels();

        foreach($models as $model)
        {
            //Parsing
            $arrayModel = Yaml::parse(file_get_contents($model));

            //Création model/repo/api_routes
            $this->registerModel($arrayModel);
            $this->registerRepository(key($arrayModel));
            
        }

    }

    public function loadYmlModels()
    {
        // Récupérer chaque fichier dans le dossier app/modules/models
        return RecursiveFilesHelper::getFilePathsByDirectory($this->modelsPath, 'yml');
    }

    public function loadJsonModels()
    {
        // Récupérer chaque fichier dans le dossier routes
        return RecursiveFilesHelper::getFilePathsByDirectory($this->modelsPath, 'json');
    }

    public function loadModules()
    {
        $this->modulesFactory->loadCoreModules();
        // $this->moduleLoader->loadCustomModules();

        // die;
    }


    public function registerRepository($name)
    {
        $this->app['repo.' . $name] = function ($app) use ($name) {
            return new GenericRepository($app['db'], $app, $name);
        };
    }


    public function registerModel($arrayModel)
    {
        //Ajout de la structure de données dans l'app (pour pouvoir la récupérer et vérifier côté controller)
        return $this->app['model.'.key($arrayModel)] = $arrayModel[key($arrayModel)];
    }


    public function getModelsPath()
    {
        return $this->modelsPath;
    }

    public function setModelsPath($modelsPath)
    {
        $this->modelsPath = $modelsPath;

        return $this;
    }

    public function getModulesFactory()
    {
        return $this->modulesFactory;
    }

    public function setModulesFactory($modulesFactory)
    {
        $this->modulesFactory = $modulesFactory;

        return $this;
    }

}
