<?php

namespace Mkch\CoreApi\Factory;

use Mkch\CoreApi\Application;

use Mkch\CoreApi\Helper\RecursiveFilesHelper;

class ModulesFactory
{
    private $app;
    private $coreModulesPath;
    private $coreModulesNamespace;
    private $routesPath;
    private $modelsPath;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->coreModulesPath = $this->app->getCorePath() . '/Modules';
        $this->coreModulesNamespace = "Mkch\\CoreApi\\Modules";

        $this->modelsPath = array();
        $this->routesPath = array();
    }


    public function loadCoreModules()
    {
        //Récupération des dossiers de Modules
        $arrayModulesPath = glob($this->coreModulesPath . '/*' , GLOB_ONLYDIR);
        // $arrayModulesPath = glob('./*' , GLOB_ONLYDIR);

        //Parsing
        foreach($arrayModulesPath as $path) {

            $moduleName = substr($path, strripos($path, '/')+1);

            // Utile surtout pour pouvoir récupérer le routePath dans le RoutesLoader
            array_push($this->modelsPath, $path . '/conf/models');
            array_push($this->routesPath, $path . '/conf/routes');

            //Chargement de chaque classe contenue dans le dossier Factory
            $factories = RecursiveFilesHelper::getFilePathsByDirectory($path . '/Factory', 'php');

            foreach($factories as $factory) {
                $class = $this->coreModulesNamespace . '\\' . $moduleName . '\\Factory\\' . substr($factory, strripos($factory, '/') +1, -4);

                //Chargement de la classe et instanciation du module
                new $class($this->app);
            }
        }
    }

    public function loadCustomModules()
    {
        //Charger conf dans src + charger routes
        //dump('loadCustomModules');

    }


    public function getApp()
    {
        return $this->app;
    }

    public function setApp($app)
    {
        $this->app = $app;

        return $this;
    }

    public function getCoreModulesPath()
    {
        return $this->coreModulesPath;
    }

    public function setCoreModulesPath($coreModulesPath)
    {
        $this->coreModulesPath = $coreModulesPath;

        return $this;
    }

    public function getCoreModulesNamespace()
    {
        return $this->coreModulesNamespace;
    }

    public function setCoreModulesNamespace($coreModulesNamespace)
    {
        $this->coreModulesNamespace = $coreModulesNamespace;

        return $this;
    }

    public function getRoutesPath()
    {
        return $this->routesPath;
    }

    public function setRoutesPath($routesPath)
    {
        $this->routesPath = $routesPath;

        return $this;
    }

    public function getModelsPath()
    {
        return $this->modelsPath;
    }

    public function setModelsPath($modelsPath)
    {
        $this->modelsPath = $modelsPath;

        return $this;
    }

}
