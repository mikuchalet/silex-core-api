<?php

namespace Mkch\CoreApi\Factory;

interface InterfaceApiFactory
{
    public function loadYmlModels();

    public function loadJsonModels();

    public function registerRepository($name);

    public function registerModel($arrayModel);
}
